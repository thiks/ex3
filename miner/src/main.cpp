#include <iostream>
#include <ctime>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/ml/ml.hpp>

#include "intToString.h"

using namespace std;
using namespace cv;

enum {
    NEGATIVE_EXAMPLES = 0,
    POSITIVE_EXAMPLES = 1
};

void load_images(string images_path, vector<Mat> *images, int image_number, int examples, int samples=0) {
    if (examples == POSITIVE_EXAMPLES) {
        for (int i = 0; i < image_number; i++) {
            string image_name = intToString(i + 1, 5);
            string positive_image_path = images_path + image_name + ".jpg";

            Mat positive_image = imread(positive_image_path, CV_LOAD_IMAGE_GRAYSCALE);
            resize(positive_image, positive_image, Size(64, 64));

            images->push_back(positive_image);
        }
    } else if (examples == NEGATIVE_EXAMPLES) {
        for (int i = 0; i < image_number; i++) {
            string image_name = intToString(i + 1, 5);
            string negative_image_path = images_path + image_name + ".jpg";

            Mat negative_image = imread(negative_image_path, CV_LOAD_IMAGE_GRAYSCALE);
            int width = negative_image.cols;
            int height = negative_image.rows;

            for (int j = 0; j < samples; j++) {
                int x = rand() % (width - 10);
                int y = rand() % (height - 10);

                int delta_x = width - x;
                int delta_y = height - y;

                int delta = (delta_x > delta_y) ? delta_y : delta_x;

                int edge_length = rand() % delta;

                if (edge_length > 200) edge_length = 200;
                if (edge_length < 10) edge_length = 10;

                Rect r(x, y, edge_length, edge_length);

                Mat cutout;
                negative_image(r).copyTo(cutout);

                resize(cutout, cutout, Size(64, 64));

                images->push_back(cutout);
            }
        }
    }
}

void prepare_data(vector<Mat> *images, Mat *training_data, Mat *training_labels,
                      int positive_images, int negative_images) {
    HOGDescriptor descriptor;
    descriptor.winSize = Size(64, 64);

    for (int i = 0; i < positive_images; i++) {
        training_labels->at<float>(0, i) = 1;

        vector<float> desc_value;
        descriptor.compute((*images)[i], desc_value);

        Mat c = Mat_<float>(desc_value, true);
        c.col(0).copyTo(training_data->col(i));
    }

    for (int i = positive_images; i < positive_images + negative_images; i++) {
        training_labels->at<float>(0, i) = -1;

        vector<float> desc_value;
        descriptor.compute((*images)[i], desc_value);

        Mat c = Mat_<float>(desc_value, true);
        c.col(0).copyTo(training_data->col(i));
    }
}

void test(vector<Mat> *images, CvSVM *svm, vector<Mat> *negative_images) {
    HOGDescriptor descriptor;
    descriptor.winSize = Size(64, 64);

    int count = 0;

    for (vector<Mat>::iterator it = images->begin(); it != images->end(); ++it) {
        vector<float> desc_value;
        descriptor.compute(*it, desc_value);

        Mat desc = Mat_<float>(desc_value, true);
        transpose(desc, desc);

        float result = svm->predict(desc, true);

        if (result < 0) {
            negative_images->push_back(*it);
            count++;
        }
    }

    cout << "False positives: " << count << " of " << images->size() << endl;
}

int main(int argc, char ** argv) {
    int iterations;
    string training_images_path;
    string output_path;

    int n_positive_images = 1000;
    int n_negative_images = 500;
    int samples = 100;

    int mining_interations = 10;

    if (argc != 4) {
        cout << "parameters missing" << endl;
        return 1;
    } else {
        training_images_path = argv[1];
        output_path = argv[2];
        iterations = atoi(argv[3]);
    }

    srand((uint)time(NULL));

    vector<Mat> positive_images;
    load_images(training_images_path + "/positive/", &positive_images, n_positive_images, POSITIVE_EXAMPLES);

    vector<Mat> negative_images;
    load_images(training_images_path + "/negative/", &negative_images, n_negative_images, NEGATIVE_EXAMPLES, samples);

    for (int i = 0; i < mining_interations; i++) {
        cout << "Iteration " << i + 1 << ":" << endl;

        n_negative_images = (int)negative_images.size();

        cout << n_positive_images << " Positive Examples" << endl;
        cout << n_negative_images << " Negative Examples" << endl;

        Mat training_labels(1, n_positive_images + n_negative_images, CV_32FC1);
        Mat training_data(1764, n_positive_images + n_negative_images, CV_32FC1);

        vector<Mat> images;
        images.insert(images.end(), positive_images.begin(), positive_images.end());
        images.insert(images.end(), negative_images.begin(), negative_images.end());

        prepare_data(&images, &training_data, &training_labels, n_positive_images, n_negative_images);

        cout << "Preparations complete!" << endl;

        CvSVMParams params;
        params.svm_type = CvSVM::C_SVC;
        params.kernel_type = CvSVM::LINEAR;
        params.term_crit = cvTermCriteria(CV_TERMCRIT_ITER, iterations, 1e-6);

        CvSVM svm;

        transpose(training_data, training_data);
        transpose(training_labels, training_labels);

        cout << "Begin training" << endl;

        svm.train(training_data, training_labels, Mat(), Mat(), params);

        cout << "End Training" << endl;

        vector<Mat> fp_examples;
        load_images(training_images_path + "/negativeBedroom/", &fp_examples, 500, NEGATIVE_EXAMPLES, 10);

        cout << "Begin testing" << endl;

        test(&fp_examples, &svm, &negative_images);

        if (i + 1 == mining_interations) {
            svm.save(output_path.c_str());
        }
    }

    return 0;
};
