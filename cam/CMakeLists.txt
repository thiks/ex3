cmake_minimum_required(VERSION 3.2)
project(cam)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Ofast -fopenmp")

find_package(OpenCV REQUIRED core highgui imgproc objdetect ml)

set(SOURCE_FILES src/main.cpp
                 src/detector.hpp
                 src/detector.cpp)

add_executable(cam ${SOURCE_FILES})

target_link_libraries(cam ${OpenCV_LIBS})